import unittest
import tp55a
import os


class Testtp55a(unittest.TestCase):
    def test(self):
        self.assertEqual(tp55a.siglar("A veces pasa"), "Avp")
        self.assertEqual(tp55a.siglar("vaca"), "v")
        self.assertEqual(tp55a.siglar("Un aplauso me asusta"), "Uama")
        self.assertEqual(tp55a.siglar("Universal Serial Bus"), "USB")


if __name__ == '__main__':
    os.system('cls')
    unittest.main()
