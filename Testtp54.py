import unittest
import tp54
import os


class Testtp54(unittest.TestCase):
    def test(self):
        self.assertEqual(tp54.capitalizar("Ppp", "ppp"), 1)
        self.assertEqual(tp54.capitalizar("HOLA", "hola"), 2)
        self.assertEqual(tp54.capitalizar("", "p"), 2)
        self.assertEqual(tp54.capitalizar("", ""), 2)


if __name__ == '__main__':
    os.system('cls')
    unittest.main()
