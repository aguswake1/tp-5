import unittest
import tp56c
import os


class Testtp56c(unittest.TestCase):
    def test(self):
        self.assertEqual(tp56c.vocalesmas("aeiou"), "eioua")
        self.assertEqual(tp56c.vocalesmas("vaca"), "vece")
        self.assertEqual(tp56c.vocalesmas("hhhkkkppp"), "hhhkkkppp")
        self.assertEqual(tp56c.vocalesmas("   ffuuu   "), "ffaaa")


if __name__ == '__main__':
    os.system('cls')
    unittest.main()
