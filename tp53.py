import time
import os


def contar(palabra):
    i = 0
    vocales = 0
    for letra in palabra:
        if (letra == "a" or letra == "A" or letra == "e" or letra == "E" or letra == "I" or letra == "i" or letra == "o" or letra == "O" or letra == "u" or letra == "U"):
            vocales = vocales + 1
    time.sleep(1)
    if vocales != 0:
        print("Las vocales que posee esta palabra son", vocales, ".")
    else:
        time.sleep(1)
        print("Esta palabra no posee vocales.")
    return (vocales)    
if __name__ == '__main__':
    os.system('cls')
    palabras = (input("Ingrese la palabra a la que le desea contar las vocales: "))
    contar(palabras)
