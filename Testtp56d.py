import unittest
import tp56d
import os


class Testtp55c(unittest.TestCase):
    def test(self):
        self.assertEqual(tp56d.palindromo("anita lava la tina"), True)
        self.assertEqual(tp56d.palindromo("Las vacas son feas"), False)


if __name__ == '__main__':
    os.system('cls')
    unittest.main()
