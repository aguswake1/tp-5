import unittest
import tp55b
import os


class Testtp55b(unittest.TestCase):
    def test(self):
        self.assertEqual(tp55b.capitalizar("hola"), "Hola")
        self.assertEqual(tp55b.capitalizar("antonio"), "Antonio")
        self.assertEqual(tp55b.capitalizar("dormir"), "Dormir")


if __name__ == '__main__':
    os.system('cls')
    unittest.main()
