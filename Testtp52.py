import unittest
import tp52
import os


class Testtp52(unittest.TestCase):
    def test(self):
        self.assertEqual(tp52.contarletras("aaaeee"), 3)
        self.assertEqual(tp52.contarletras("aae"), 1)
        self.assertEqual(tp52.contarletras("aee"), 2)
        self.assertEqual(tp52.contarletras(""), 4)


if __name__ == '__main__':
    os.system('cls')
    unittest.main()
