import os
os.system('cls')


def AS(frase):
    frase = frase + " "  # LE agrego un espacio al final a la frace ingresada para darle un stop al While mas abajo
    palabras = ""
    i = 0
    for i in range(len(frase)):
        if ((frase[i] == "a" or frase[i] == "A") and frase[i-1] == " "):
            j = i
            while frase[j] != " ":  # Si encuentras una a o A despues de un espacio durante el escaneo del string guarda cada caracter hasta detener siguiente hasta el proximo espacio
                palabras = palabras + frase[j]
                j = j + 1
            palabras = palabras + " "
    return (palabras)
if __name__ == "__main__":
    frases = input("Ingrese la frase que desea abreviar: ")
    frases = AS(frases)
    print(frases)
