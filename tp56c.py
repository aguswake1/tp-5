import os
os.system('cls')


def vocalesmas(frase):
    cambio = ""
    for letra in frase:  # Escaneo todo el string guardando cada letra en cada posición en "letra"
        if (letra == "a"):  # Si encuantra una vocal guarda en su posicion la vocal siguiente pero de la variable Cambio
            cambio = cambio + "e"
        elif (letra == "A"):
            cambio = cambio + "E"
        elif (letra == "e"):
            cambio = cambio + "i"
        elif (letra == "E"):
            cambio = cambio + "I"
        elif (letra == "i"):
            cambio = cambio + "o"
        elif (letra == "I"):
            cambio = cambio + "O"
        elif (letra == "o"):
            cambio = cambio + "u"
        elif (letra == "O"):
            cambio = cambio + "U"
        elif (letra == "u"):
            cambio = cambio + "a"
        elif (letra == "U"):
            cambio = cambio + "A"
        elif (letra == " "):
            cambio = cambio + ""
        else:
            cambio = cambio + letra  # Si la letra no es vocal no es nesesario el cambio y se guarda la misma letra
    return(cambio)
if __name__ == '__main__':    
    frases = input("Escriba la oración: ")
    frases = vocalesmas(frases)
    print(frases)
