import unittest
import tp56b
import os


class Testtp56b(unittest.TestCase):
    def test(self):
        self.assertEqual(tp56b.solovocales("A veces pasa"), "Aeeaa")
        self.assertEqual(tp56b.solovocales("vaca"), "aa")
        self.assertEqual(tp56b.solovocales("Un aplaso me asusta"), "Uaaoeaua")
        self.assertEqual(tp56b.solovocales("cccvvvttt"), "")


if __name__ == '__main__':
    os.system('cls')
    unittest.main()
