import unittest
import tp56a
import os


class Testtp56a(unittest.TestCase):
    def test(self):
        self.assertEqual(tp56a.soloconsonantes("A veces pasa"), "vcsps")
        self.assertEqual(tp56a.soloconsonantes("vaca"), "vc")
        self.assertEqual(tp56a.soloconsonantes("Un aplauso me asusta"), "nplsmsst")
        self.assertEqual(tp56a.soloconsonantes("aaaa aeeeiiiaaa"), "")


if __name__ == '__main__':
    os.system('cls')
    unittest.main()
