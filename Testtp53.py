import unittest
import tp53
import os


class Testtp53(unittest.TestCase):
    def test(self):
        self.assertEqual(tp53.contar("Murcielago"), 5)
        self.assertEqual(tp53.contar("vaca"), 2)
        self.assertEqual(tp53.contar("mmmsssdd"), 0)
        self.assertEqual(tp53.contar("RICARDO"), 3)


if __name__ == '__main__':
    os.system('cls')
    unittest.main()
