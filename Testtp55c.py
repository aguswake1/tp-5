import unittest
import tp55c
import os


class Testtp55c(unittest.TestCase):
    def test(self):
        self.assertEqual(tp55c.AS("vaca"), "")
        self.assertEqual(tp55c.AS("Un aplauso me asusta"), "aplauso asusta ")
        self.assertEqual(tp55c.AS("saaaa baaaa"), "")


if __name__ == '__main__':
    os.system('cls')
    unittest.main()
