import os
import time


def agendar(Nombre):
    agenda = {'Carlos': 44577708, 'Marcos': 46262359, 'Juan': 55555555, 'Leonel': 1132305939}  # Declaro el diccionario agenda
    terminar = " "  # Variable que usaré para verificar si quiere cerrar el programa
    correcto = "s"  # Variable para verificar si el numero en la agenda es el correcto
    while (terminar != "*"):  # "mientras que me asegura que el proceso termina con el ngreso de un *"
        if (Nombre in agenda):  # Verificamos que el nombre ingresado está en el diccionario declarado como agenda
            esta = True
            print(agenda[Nombre])
            correcto = input("¿Es correcto? [s] o [n] ")
            while (correcto != "s"):  # Me aseguro que el usuario solo pueda ingresar s para verificar o n para negar
                if (correcto == "n"):
                    agenda[Nombre] = input("Ingrese el número correcto: ")  # Si es numero en la agenda no es correcto puede cambiarlo
                    correcto = "s"  # Salgo del mientras
                else:
                    correcto = input("Respuesta incorrecta\n¿Es correcto? [s] o [n] ")
        else:
            esta = False
            numero = input("No se encuentra agendado.\n Ingresemos su telefono: ")
            agenda[Nombre] = numero  # Si el nombre ingresado no se encuentra en la agenda se crea una entrada y el usuario puede ingresar el numero
            print("Telefono de", Nombre, "ingresado")
        time.sleep(2)
        os.system('cls')
        terminar = input("Ingrese * para terminar, ENTER para continuar")  # Se pregunta si quiere terminar el proceso con *
    print("terminado")
    return (esta)

if __name__ == "__main__":
    print("BIENVENIDO\n Nesesitamos un nombre para verificar en la agenda,\n recuerde que se discrimina entre mayusculas y minusculas.")
    Nombres = input("Ingrese el nombre a buscar en la agenda: ")
    agendar(Nombres)